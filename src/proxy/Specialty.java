package proxy;

public interface Specialty {
    void display();
}
