package iterator;

public  interface Aggregate {//抽象聚合
    public void add(Object obj);
    public void remove(Object obj);
    public Iterator getIterator();
}
