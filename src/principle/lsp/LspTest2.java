package principle.lsp;

/**
 * @ClassName: LspTest2
 * @Description:
 * @Author: CHonghao
 * @Date: 2021/2/1 12:56
 * @Version: V1.0
 **/
public class LspTest2 {
    public static void main(String[] args) {
        Animal bird_swallow = new Bird_Swallow();
        Animal notBird_brownKiwi = new NotBird_BrownKiwi();
        bird_swallow.setSpeed(120);
        notBird_brownKiwi.setSpeed(100);
        System.out.println("如果移动300公里：");
        try {
            System.out.println("燕子将花费" + bird_swallow.getFlyTime(300) + "小时。"); // 燕子将花费2.5小时。
            System.out.println("几维鸟将花费" + notBird_brownKiwi.getFlyTime(300) + "小时。"); // 几维鸟将花费2.5小时。
        } catch (Exception err) {
            System.out.println("发生错误了！");
        }
    }
}

// 动物类
class Animal {
    private double speed;

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getFlyTime(double distance) {
        return (distance / speed);
    }
}

// 燕子类
class Bird_Swallow extends Animal {
}

// 几维鸟类
class NotBird_BrownKiwi extends Animal {

}
