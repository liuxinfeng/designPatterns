package construct.absfactory;



 import java.awt.*;
 import javax.swing.*;

public class FarmTest {
    public static void main(String[] args) {
        try {

            Farm f = (Farm) ReadXML.getObject();
            Animal a = f.newAnimal();
            Plant p = f.newPlant();
            a.show();
            p.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}