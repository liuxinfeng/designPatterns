package construct.BuilderPattern;

public interface Packing {

	public String pack();
}
