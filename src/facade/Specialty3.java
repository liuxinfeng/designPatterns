package facade;

import javax.swing.ImageIcon;

public class Specialty3 extends ImageIcon {
    private static final long serialVersionUID = 1L;
    Specialty3() {
        super("src/facade/Specialty13.jpeg");
    }
}
