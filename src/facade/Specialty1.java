package facade;

import javax.swing.ImageIcon;

public class Specialty1 extends ImageIcon{
    private static final long serialVersionUID = 1L;
    Specialty1() {
        super("src/facade/Specialty11.jpeg");
    }
}
