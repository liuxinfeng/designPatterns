package facade;
import javax.swing.ImageIcon;

public class Specialty4 extends ImageIcon {
    private static final long serialVersionUID = 1L;
    Specialty4() {
        super("src/facade/Specialty14.jpeg");
    }
}
