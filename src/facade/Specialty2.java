package facade;

import javax.swing.ImageIcon;

public class Specialty2 extends ImageIcon{
    private static final long serialVersionUID = 1L;
    Specialty2() {
        super("src/facade/Specialty12.jpeg");
    }
}
