package simplefactory.pizzastore.pizza;

public abstract class Pizza {

    protected String name;//名字

    //准备原材料，不同的披萨不一样的，因此，写成抽象方法
    public abstract void prepare();

    public void bake() {
        System.out.println(name + "baking;");
    }

    public void cut() {
        System.out.println(name + "cut;");
    }

    public void box() {
        System.out.println(name + "box;");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
